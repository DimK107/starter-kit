import { NgxCropperModule } from './ngx-cropper/ngx-cropper.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentErrorHandlerComponent } from './component-error-handler/component-error-handler.component';

@NgModule({
  declarations: [AppComponent, ComponentErrorHandlerComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCropperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
