import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ng-app';
  name = 1;
  imageSrc;
  fileChangedEvent: any;
  data = {};

  constructor() {}

  ngOnInit(): void {}

  onChangeFile(event) {
    this.fileChangedEvent = event;
  }

  imageCropped(event) {
    this.imageSrc = event;
  }
}
