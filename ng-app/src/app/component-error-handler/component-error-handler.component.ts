import { Component, OnInit, ViewContainerRef, Inject } from '@angular/core';

@ClassErrorHandler(['methodWithAnotherError'])
@Component({
  selector: 'app-component-error-handler',
  templateUrl: './component-error-handler.component.html',
  styleUrls: ['./component-error-handler.component.scss']
})
export class ComponentErrorHandlerComponent implements OnInit {
  ref;
  constructor(@Inject(ViewContainerRef) viewContainerRef) {
    this.ref = viewContainerRef;
  }

  ngOnInit() {}

  @MethodErrorHandler()
  methodWithError() {
    throw Error('Some error');
  }

  methodWithAnotherError() {
    throw Error('Some another error');
  }
}

export function MethodErrorHandler(hideElementOnError: boolean = false) {
  // tslint:disable-next-line:only-arrow-functions
  return function(
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalFn = descriptor.value.bind(target);
    descriptor.value = () => {
      try {
        originalFn();
      } catch (error) {
        console.log('================= ERROR', error);
      }
    };
  };
}

function ClassErrorHandler(functionsToDecorate: string[] = []) {
  return target => {
    functionsToDecorate.forEach(name => {
      let self: Component & { ref: ViewContainerRef };
      let originalFn = target.prototype[name];

      if (target.prototype[name]) {
        target.prototype[name] = function(...args) {
          try {
            self = this;
            originalFn = originalFn.bind(this);
            originalFn();
          } catch (error) {
            try {
              debugger;
              const mountElement: HTMLElement = self.ref.element.nativeElement;
              // TODO: log error, send report, etc
              self = null;
              mountElement.innerHTML =
                'Sorry, Something went wrong. We will fix it in one of the next releases. Please contact us';
            } catch (error) {
              console.log('================= ERROR', error);
            }
          }
        };
      }
    });

    return target;
  };
}
