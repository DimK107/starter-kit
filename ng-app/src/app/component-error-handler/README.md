### Features

-

- Remove component from UI/DOM add text;

# Decorator for class

    @ClassErrorHandler(['methodWithAnotherError']) // method name to decorate
    @Component({
    	selector: 'app-component-error-handler',
    	templateUrl: './component-error-handler.component.html',
    	styleUrls: ['./component-error-handler.component.scss']
    	})
    	export class ComponentErrorHandlerComponent implements OnInit {
    		ref;
    		constructor(@Inject(ViewContainerRef) viewContainerRef) { // Needed for removing element from DOM
    			this.ref = viewContainerRef;
    		}
    		ngOnInit() {}

    		methodWithAnotherError() {
    			throw Error('Some another error');
    		}
    	}

# Decorator for method

    @Component({
    	selector: 'app-component-error-handler',
    	templateUrl: './component-error-handler.component.html',
    	styleUrls: ['./component-error-handler.component.scss']
    	})
    	export class ComponentErrorHandlerComponent implements OnInit {
    		ref;
    		constructor(@Inject(ViewContainerRef) viewContainerRef) {
    			this.ref = viewContainerRef;
    		}
    		ngOnInit() {}

    		@MethodErrorHandler()
    		methodWithError() {
    			throw Error('Some error');
    		}
    	}
