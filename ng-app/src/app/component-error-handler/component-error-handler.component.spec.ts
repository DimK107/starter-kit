import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentErrorHandlerComponent } from './component-error-handler.component';

describe('ComponentErrorHandlerComponent', () => {
  let component: ComponentErrorHandlerComponent;
  let fixture: ComponentFixture<ComponentErrorHandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentErrorHandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentErrorHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
