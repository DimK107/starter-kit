import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { AppLogger } from './utils/logger';

@Controller()
export class AppController {
  private readonly logger = new AppLogger(AppController.name);

  constructor(private readonly appService: AppService) {}

  @Get()
  getVersion(): Promise<string> {
    return this.appService.getApiVersion();
  }
}
