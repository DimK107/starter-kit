import { ApiModelProperty } from '@nestjs/swagger';

const TOKEN_EXAMPLE: string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
    + 'eyJpZCI6MiwiZW1haWwiOiJ0ZXN0MUB0ZXN0LmNvbSIsIm5hbWUiOiJKb2huIERvZSIsImFjY2Vzc0xldmVsIjowLCJpYXQiOjE1NTA0ODg3NDUsImV4cCI6MTU1MDU3NTE0NX0'
    + 'gBWeKwFUa-7IQPDvZ8UgX6ixHKuv0jKPSSoOgdAy1VM';

export class AuthResponseDto {

    @ApiModelProperty({ example: '1d' })
    readonly expiresIn: string;

    @ApiModelProperty({ example: TOKEN_EXAMPLE })
    readonly accessToken: string;
}
