import { IsString, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterDto {

    @IsString()
    @ApiModelProperty() readonly name: string;

    @IsEmail()
    @ApiModelProperty() readonly email: string;

    @IsString()
    @ApiModelProperty() readonly password: string;

}
