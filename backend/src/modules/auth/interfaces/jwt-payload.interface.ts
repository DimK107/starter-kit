export interface JwtPayload {
  id: number | string;
  email: string;
  name: string;
}
