import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { User } from '../user/user.entity';
import { AuthResponse } from './interfaces/auth-response.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(payload: JwtPayload): Promise<User> {
    return this.userService.getById(payload.id as number);
  }

  async createToken(payload: JwtPayload): Promise<string> {
    return this.jwtService.sign(payload);
  }
}
