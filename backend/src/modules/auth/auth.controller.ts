import { comparePassword } from './../../utils/password';
import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { LoginDto } from './dto/login.dto';
import { AuthResponse } from './interfaces/auth-response.interface';
import { AuthResponseDto } from './dto/auth.response.dto';
import { RegisterDto } from './dto/register.dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('login')
  @ApiOperation({ title: 'Login user' })
  @ApiResponse({ status: 200, type: AuthResponseDto })
  async login(@Body() { email, password }: LoginDto): Promise<AuthResponse> {
    const user = await this.userService.findByEmail(email, true);

    if (!user) {
      throw new HttpException(
        'Incorrect email or password',
        HttpStatus.NOT_FOUND,
      );
    }

    const isPasswordValid = await comparePassword(password, user.password);

    if (!isPasswordValid) {
      throw new HttpException('Password is incorrect', HttpStatus.UNAUTHORIZED);
    }

    const accessToken = await this.authService.createToken({
      id: user.id,
      email: user.email,
      name: user.name,
    });

    return { accessToken };
  }

  @Post('register')
  @ApiOperation({ title: 'Register user' })
  @ApiResponse({ status: 200, type: AuthResponseDto })
  async register(@Body() { name, email, password }: RegisterDto): Promise<
    AuthResponse
  > {
    const user = await this.userService.findByEmail(email);

    if (user) {
      throw new HttpException('Email already taken', HttpStatus.CONFLICT);
    }

    const newUser = await this.userService.create({
      name,
      email,
      password,
    });

    const accessToken = await this.authService.createToken({
      id: newUser.id,
      email: newUser.email,
      name: newUser.name,
    });

    return { accessToken };
  }
}
