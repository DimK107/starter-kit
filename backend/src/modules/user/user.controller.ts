import {
  Controller,
  Get,
  Req,
  Post,
  Body,
  Param,
  Delete,
  HttpCode,
  Query,
  Put,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
// import { IntPipe } from '../common/pipes/int.pipe';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';
import { AuthGuard } from '@nestjs/passport';
import { UserDto } from './dto/user.dto';

@ApiUseTags('users')
@Controller('users')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class UsersController {
  constructor(private readonly userService: UserService) {}

  // @Get()
  // @ApiOperation({ title: 'Get all users' })
  // @ApiResponse({ status: 200, isArray: true, type: UserDto })
  // getAllUsers(@Query() query: FindQueryDto): Promise<User[]> {
  //     return this.userService.getAll(query);
  // }

  //   @Get('/me')
  //   @ApiOperation({ title: 'Get user by token' })
  //   @ApiResponse({ status: 200, type: UserDto })
  //   getUserByToken(@Req() req): Promise<User> {
  //     const { id } = req.user;
  //     return this.userService.getById(id);
  //   }

  //   @Get('/:id')
  //   @ApiOperation({ title: 'Get user by id' })
  //   @ApiResponse({ status: 200, type: UserDto })
  //   getUser(@Param('id', new IntPipe()) userId: number): Promise<User> {
  //     return this.userService.getById(userId);
  //   }

  //   @Post()
  //   @ApiOperation({ title: 'Create a new user' })
  //   @ApiResponse({ status: 201, type: UserDto })
  //   async create(@Body() userDto: CreateUserDto) {
  //     const user = await this.userService.findByEmail(userDto.email);

  //     if (user) {
  //       throw new BadRequestException('User with this email already registered');
  //     }

  //     return this.userService.create(userDto);
  //   }

  //   @Put('/:id')
  //   @ApiOperation({ title: 'Update user information' })
  //   @ApiResponse({ status: 200, type: UserDto })
  //   async update(
  //     @Body() userDto: UpdateUserDto,
  //     @Param('id', new IntPipe()) id: number,
  //   ): Promise<User> {
  //     const user = await this.userService.findByEmail(userDto.email);

  //     if (user && id !== user.id) {
  //       throw new BadRequestException('User with this email already registered');
  //     }

  //     return this.userService.update(id, userDto);
  //   }

  //   @Delete('/:id')
  //   @HttpCode(204)
  //   @ApiOperation({ title: 'Delete a user' })
  //   @ApiResponse({ status: 204, description: 'Successfully deleted user' })
  //   remove(@Param('id', new IntPipe()) userId: number): Promise<void> {
  //     return this.userService.delete(userId);
  //   }
}
