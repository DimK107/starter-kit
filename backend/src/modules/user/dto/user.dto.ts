import { ApiModelProperty } from '@nestjs/swagger';

export class UserDto {
  @ApiModelProperty({ example: 1 })
  readonly id: number;

  @ApiModelProperty({ example: 'mail@example.com' })
  readonly email: string;

  @ApiModelProperty({ example: 'John Doe' })
  readonly name?: string;

  @ApiModelProperty({ example: '2019-02-18T10:44:16.412Z' })
  readonly createdAt: Date;

  @ApiModelProperty({ example: '2019-02-18T10:44:16.412Z' })
  readonly updatedAt: Date;
}
