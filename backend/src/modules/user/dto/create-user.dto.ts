import { UpdateUserDto } from "./update-user.dto";
import { IsString, IsNotEmpty } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";

export class CreateUserDto extends UpdateUserDto {
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    readonly password: string;
}
