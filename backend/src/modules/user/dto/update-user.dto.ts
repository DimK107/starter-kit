import {
  IsString,
  IsEmail,
  IsOptional,
  IsNotEmpty,
  IsNumber,
} from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiModelProperty({
    example: 'test@test.com',
  })
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelPropertyOptional({
    example: 'John Doe',
  })
  readonly name: string;
}
