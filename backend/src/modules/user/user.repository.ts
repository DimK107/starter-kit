import { EntityRepository, Repository, UpdateResult } from 'typeorm';
import { User } from './user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import passport = require('passport');

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  // findAllUsers(page, itemsOnPage, orderBy, orderType, search): Promise<User[]> {
  //   const sqlQuery = this.createQueryBuilder('user');

  //   if (!!search) {
  //     sqlQuery.where('user.name like :name', { name: `%${search}%` });
  //     sqlQuery.orWhere('user.email like :email', { email: `%${search}%` });
  //   }

  //   sqlQuery.leftJoinAndSelect('user.organization', 'organization');

  //   const order = {};
  //   order[`user.${orderBy}`] = orderType || QueryOrder.DESC;

  //   sqlQuery
  //     .orderBy(order)
  //     .skip(page * itemsOnPage)
  //     .take(itemsOnPage);

  //   return sqlQuery.getMany();
  // }

  findByEmail(email: string, returnPassword: boolean = false): Promise<User> {
    // TODO: user keys
    return this.findOne({ email }, { select: ['email', 'name'] });
  }

  updateUser(id, user: UpdateUserDto): Promise<UpdateResult> {
    const userUpdates: Partial<User> = {
      name: user.name,
    };
    return this.update(id, userUpdates);
  }
}
