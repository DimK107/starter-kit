import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './user.controller';
import { UserService } from './user.service';
import { PasswordService } from './password.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import * as mockRepository from './mock';

describe('User Controller', () => {
  let controller: UsersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UserService,
        PasswordService,
        { 
          provide: getRepositoryToken(UserRepository),
          useValue: mockRepository
        }
      ]
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
