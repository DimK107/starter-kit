import { DbBaseEntity } from './../../common/entity/base.entity';
import { Entity, Column } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity()
export class User extends DbBaseEntity {
  @Column({ length: 255, unique: true })
  email: string;

  @Column({ select: false })
  @Exclude()
  password: string;

  @Column({ length: 50 })
  name: string;
}
