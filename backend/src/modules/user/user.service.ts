import { generatePassword } from './../../utils/password';
import { UserRepository } from './user.repository';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  async getById(id: number): Promise<User> {
    return this.userRepository.findOneOrFail(id);
  }

  async create(userDto: CreateUserDto): Promise<User> {
    const password = await generatePassword(userDto.password);
    const newUser = await this.userRepository.save({ ...userDto, password });

    return this.getById(newUser.id);
  }

  async update(id: number, user: UpdateUserDto): Promise<User> {
    await this.userRepository.updateUser(id, user);
    return this.getById(id);
  }

  async delete(id: number): Promise<void> {
    await this.userRepository.delete(id);
  }

  async findByEmail(
    email: string,
    withPassword: boolean = false,
  ): Promise<User> {
    return this.userRepository.findByEmail(email, withPassword);
  }
}
