import { LoggerService } from '@nestjs/common';
import { Logger, format, transports, createLogger } from 'winston';

export class AppLogger implements LoggerService {
  private logger: Logger;
  private context: string = 'DEFAULT';

  constructor(context: string = 'DEFAULT') {
    this.context = context;
    this.logger = createLogger({
      defaultMeta: { CONTEXT: this.context },
      transports: this.getTransports(),
    });
  }

  log(message: string, level: string = 'info') {
    this.logger.log({ level, message });
  }
  error(message: string, trace?: string) {
    this.logger.error(message);
  }
  warn(message: string) {
    this.logger.warn(message);
  }

  info(message: string) {
    this.logger.info(message);
  }

  private getTransports() {
    return [
      new transports.File({ filename: 'logs/error.log', level: 'error' }),
      new transports.File({ filename: 'logs/combined.log' }),
      new transports.Console({
        format: format.combine(
          format.timestamp(),
          format.colorize(),
          format.simple(),
        ),
      }),
    ];
  }
}
