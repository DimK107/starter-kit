import * as bcrypt from 'bcryptjs';

export const comparePassword = (
  password: string,
  hash: string,
): Promise<boolean> => bcrypt.compare(password, hash);

export const generatePassword = (
  password: string,
  saltRounds = 10,
): Promise<string> => bcrypt.hash(password, saltRounds);
