import { AppLogger } from './../../utils/logger';
import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private readonly logger = new AppLogger('REQUEST_LOGGER');

  use(req: Request, res: Response, next: () => void) {
    // TODO: add info for each request
    this.logger.log(`Request URL = "${req.url}"`);
    next();
  }
}
