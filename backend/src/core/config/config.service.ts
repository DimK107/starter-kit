import { AppLogger } from './../../utils/logger';
import * as Joi from 'joi';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import { ConnectionOptions } from 'typeorm';
import { JwtModuleOptions } from '@nestjs/jwt';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly logger = new AppLogger(ConfigService.name);
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.parse(
      fs.readFileSync(`${process.cwd()}/docker/environments`),
    );

    this.envConfig = this.validateInput(config);
  }

  getTypeormConnectionOptions(): ConnectionOptions {
    return {
      type: 'postgres',
      host: this.envConfig.DB_HOST,
      port: Number(this.envConfig.DB_PORT),
      username: this.envConfig.DB_USER,
      password: this.envConfig.DB_PASSWORD,
      database: this.envConfig.DB_NAME,
      entities: [process.cwd() + '/dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    };
  }

  getJwtModuleConfig(): JwtModuleOptions {
    return {
      secretOrPrivateKey: this.envConfig.JWT_SECRET,
      signOptions: {
        expiresIn: this.envConfig.JWT_EXPIRES,
      },
    };
  }

  getApiVersion(): string {
    return this.envConfig.API_VERSION;
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['local', 'development', 'production', 'test'])
        .default('development'),
      LOG_LEVEL: Joi.string()
        .valid(['debug'])
        .default('debug'),
      PORT: Joi.number().default(5000),
      DB_HOST: Joi.string().required(),
      DB_PORT: Joi.string().required(),
      DB_USER: Joi.string().required(),
      DB_PASSWORD: Joi.string().required(),
      DB_NAME: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),
      JWT_EXPIRES: Joi.string().required(),
      API_VERSION: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      this.logger.error(`Config validation error: ${error.message}`);
      process.exit(1);
    }
    return validatedEnvConfig;
  }
}
