import { ConfigService } from './core/config/config.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(private readonly config: ConfigService) {}

  async getApiVersion(): Promise<string> {
    return this.config.getApiVersion();
  }
}
