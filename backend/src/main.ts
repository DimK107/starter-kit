import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import 'reflect-metadata'; // Need for class-transformer
import { ValidationPipe } from '@nestjs/common';
// import { AllExceptionsFilter } from './common/filters/exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('API docs')
    .setDescription('The API description')
    .setVersion('1.0')
    .addTag('api')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/api/docs', app, document);

  // const httpRef = app.get(HTTP_SERVER_REF);
  // app.useGlobalFilters(new AllExceptionsFilter(httpRef));
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(5000); // TODO: port
}
bootstrap();
