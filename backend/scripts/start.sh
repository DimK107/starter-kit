#!/bin/sh
# NOTE: Use "local" environment only for local development
echo "================================ Starting the app..."

if [[ "$NODE_ENV" = "local" ]]; then
    npm start
else
    npm start
fi